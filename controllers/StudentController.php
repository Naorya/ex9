<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Student;//שיכיר את המודל

class StudentController extends Controller
{
 
	 public function actionView($id)
    {
		$name = Student::getName($id);//ככה קוראים לפונקציה סטטית בפי איצ' פי
		//echo "Student controller worked";
		return $this->render('view',['name' => $name]);//
    }
}